#!/usr/bin/env python
from dataclasses import dataclass
from os import path, getcwd, getenv
import shutil

import gitlab
from git import Repo
import click


@dataclass
class ProjectMetadata:
    id: int
    path: str
    name: str
    description: str
    slug: str

    def group_id(self):
        return f"com.chargerrobotics.common{f'.{self.path}' if self.path else ''}"

    def artifact_id(self):
        return self.slug


def get_project_metadata(project_id):
    gl = gitlab.Gitlab()
    proj = gl.projects.get(project_id)
    return ProjectMetadata(
        id=project_id,
        path=".".join(proj.namespace["full_path"].split("/")[2:]),
        name=proj.name,
        description=proj.description,
        slug=proj.path,
    )


REPLACEMENTS = {
    "com.chargerrobotics.common.template": lambda meta: meta.group_id() + f'.{meta.slug}',
    "_java-library": lambda meta: '_' + meta.slug,
    "/java-library": lambda meta: '/' + meta.slug,
    "Java Library Template": lambda meta: meta.name,
    "Java Library": lambda meta: meta.name,
    "<groupId>": lambda meta: meta.group_id(),
    "<artifactId>": lambda meta: meta.artifact_id(),
    "<description>": lambda meta: meta.description,
    "<sonarProjectKey>": lambda meta: meta.slug,
    "<sonarProjectName>": lambda meta: meta.name,
}


# Update lib/build.gradle
def update_build_gradle(proj_meta):
    with open("lib/build.gradle", "r") as build_gradle:
        build_content = build_gradle.readlines()

        new_content = []
        for build_line in build_content:
            for key, val in REPLACEMENTS.items():
                if key in build_line:
                    print(f"Replacing {key}")
                    build_line = build_line.replace(key, val(proj_meta))

            new_content.append(build_line)

        with open("lib/build.gradle", "w") as new_build:
            new_build.writelines(new_content)


# README.md
def update_readme(proj_meta):
    with open("README.md", "r") as readme:
        readme_content = readme.readlines()

        new_content = []
        for readme_line in readme_content:
            ## Update Gradle sample
            for key, val in REPLACEMENTS.items():
                if key in readme_line:
                    print(f"Replacing {key}")
                    readme_line = readme_line.replace(key, val(proj_meta))

            new_content.append(readme_line)

        with open("README.md", "w") as new_readme:
            new_readme.writelines(new_content)


# settings.gradle
def update_settings_gradle(proj_meta):
    with open("settings.gradle", "r") as settings_gradle:
        settings_content = settings_gradle.readlines()

        new_content = []
        for settings_line in settings_content:
            ## Update Gradle sample
            for key, val in REPLACEMENTS.items():
                if key in settings_line:
                    print(f"Replacing {key}")
                    settings_line = settings_line.replace(key, val(proj_meta))

            new_content.append(settings_line)

        with open("settings.gradle", "w") as new_settings:
            new_settings.writelines(new_content)


# .sonarlint/connectedMode.json
def update_sonarlint(proj_meta):
    with open(".sonarlint/connectedMode.json", "r") as sonar_settings:
        sonar_content = sonar_settings.readlines()

        new_content = []
        for sonar_line in sonar_content:
            ## Update Gradle sample
            for key, val in REPLACEMENTS.items():
                if key in sonar_line:
                    print(f"Replacing {key}")
                    sonar_line = sonar_line.replace(key, val(proj_meta))

            new_content.append(sonar_line)

        with open(".sonarlint/connectedMode.json", "w") as new_sonar:
            new_sonar.writelines(new_content)

def remove_update_job_from_ci():
    with open(".gitlab-ci.yml", "r") as glci:
        ci_content = glci.readlines()

        with open(".gitlab-ci.yml", "w") as new_glci:
            new_glci.writelines(ci_content[:4])

def remove_update_script():
    shutil.rmtree('pysrc')

def create_update_mr(proj_meta):
    branch_name = "feature/TA-17"
    repo = Repo(getcwd())

    repo.git.checkout(b=branch_name)
    repo.git.add(all=True)
    repo.index.commit("Perform initial repository creation updates")
    repo.git.push(branch_name, set_upstream=origin)
    remote_url = getenv("CI_REPOSITORY_URL").replace('gitlab-ci-token', 'REPO_MGMT').replace(getenv('CI_JOB_TOKEN'), getenv('REPO_MGMT'))
    repo.remotes[0].set_url(remote_url).push(branch_name)

    gl = gitlab.Gitlab(private_token=getenv('REPO_MGMT'))
    proj = gl.projects.get(proj_meta.id)
    proj.mergerequests.create({
        'source_branch': branch_name,
        'target_branch': 'main',
        'title': 'Perform Initial Repo Setup',
    })



@click.command()
@click.option("--project-id", help="GitLab project ID", required=True)
def configure(project_id):
    meta = get_project_metadata(project_id)
    update_build_gradle(meta)
    update_readme(meta)
    update_settings_gradle(meta)
    update_sonarlint(meta)
    remove_update_job_from_ci()
    remove_update_script()
    create_update_mr(meta)


if __name__ == "__main__":
    configure()
